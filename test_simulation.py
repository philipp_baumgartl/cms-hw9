#!/usr/bin/env python3

"""
Test Module for testing The Monty Hall problem and the Drunkard's Walk

The Drunkard's Walk
Read up on random walks and simulate a 2 dimensional walk ("Drunkard's Walk") over 100 intersections.

The Monty Hall problem
Read up on the Monty Hall problem and simulate it. Run 10000 loops to get a confident result. 

"""
import unittest
import simulation


class TestSimulation(unittest.TestCase):
    """
    The Python unit testing framework, sometimes referred to as “PyUnit,” is a Python language version of JUnit, by Kent Beck and Erich Gamma. JUnit is, in turn, a Java version of Kent’s Smalltalk testing framework. Each is the de facto standard unit testing framework for its respective language.
    """
    def setUp(self):
        self.n = 100
        self.nn = 10000
        self.want_to_swap = 'yes'
        self.want_not_to_swap = 'no'
        self.prices_to_win = ["car","goat","goat"]

        
    def test_random_walk(self):
        self.assertTrue(simulation.random_walk(self.n))



    def test_montyhall(self):
        self.assertTrue(simulation.montyhall(self.nn, self.want_to_swap))

    def test_montyhall_bigger_50percent(self):
        self.assertTrue(simulation.montyhall(self.nn, self.want_to_swap)>5000)
    
    def test_montyhall_smaler_50percent(self):
        self.assertTrue(simulation.montyhall(self.nn, self.want_not_to_swap)<5000)

                        
        #self.assertEqual(comprehensions.divisors(self.number),[1,2,3,6])
    

if __name__ == "__main__":
    unittest.main()