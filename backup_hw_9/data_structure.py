#!/usr/bin/env python3

"""
Pick a data structure like matrix, linked list, doubly linked list or graph. Implement this data structure as Python class with a set of useful methods and useful behavior.
E.g. a matrix should be constructed with a given number of rows and columns initialized to a value of choice. It should have methods/ properties returning the number of rows and columns. Also, it should be possible to iterate over its rows/ the row elements. You may also add methods/ operations to add one matrix to another, multiply it with a scalar or return the sum of all elements in the matrix.

"""


