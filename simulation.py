#!/usr/bin/env python3

"""
Solves all of the following problems.

    The Drunkard's Walk
    Read up on random walks and simulate a 2 dimensional walk ("Drunkard's Walk") over 100 intersections.
    The Monty Hall problem
    Read up on the Monty Hall problem and simulate it. Run 10000 loops to get a confident result.
"""

import random






def montyhall(n=1,want_to_swap = "y"):
    """simulates the Monty Hall problem"""
    
    counter_win = 0
    prices_to_win = ["car","goat","goat"]
    
    def algorithm(prices_to_win):
        """algorithm to solve the Monty Hall problem, needed for function montyhall(.)"""
        
        doors = []
        prices = []
        decision = 1
        goats = []
        for elem in prices_to_win:
            if len(prices) == 0:
                prices = prices_to_win[:]
            door = random.choice(prices)
            doors.append(door)
            prices.remove(door)

        for index in range(0,len(doors)):
            if doors[index] == "goat":
                goats.append(index)
            if doors[index] == "goat" and index != decision:
                opened_door = index
        removed_decisions = [decision, opened_door]
        swap = [x for x in [0,1,2] if x not in removed_decisions]
        
        if want_to_swap.lower() in ["yes","y"]:
            decision = swap[0]
        
        if decision not in goats:
            return 1
    
    
    for index in range(n):
        if algorithm(prices_to_win) == 1:
            counter_win += 1
    if want_to_swap.lower() in ["yes","y"]:
        print("You decided to swap")
    else:
        print("You decided not to swap")
    return counter_win

def random_walk(n=1):
    """
    simulates the Drunkard's Walk in 2 dimensions
    """
    
    x_dir = 0
    y_dir = 0
    for index in range(n):
        z = random.random()
        if z <= 0.25:
            x_dir -= 1
        elif 0.25 < z <= 0.5:
            x_dir += 1
        elif 0.5 < z <= 0.75:
            y_dir -= 1
        elif 0.75 < z <= 1:
            y_dir += 1
    return x_dir, y_dir



def main():
    print(montyhall(10000,"yes"))
    print(random_walk(100))


if __name__ == "__main__":
    sys.exit(main())