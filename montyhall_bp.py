#!/usr/bin/env python3

import random

prices_to_win = ["car","goat","goat"]

def algorithm(prices_to_win):
    doors = []
    prices = []
    decision = 1
    goats = []
    for elem in prices_to_win:
        if len(prices) == 0:
            prices = prices_to_win[:]
        door = random.choice(prices)
        doors.append(door)
        prices.remove(door)

    for index in range(0,len(doors)):
        if doors[index] == "goat":
            goats.append(index)
    for door in range(0,len(doors)):
        if doors[door] == "goat" and door != decision:
            opened_door = door
    removed_decisions = [decision, opened_door]
    swap = [x for x in [0,1,2] if x not in removed_decisions]
    
    if swap[0] not in goats:
        return 1


def simulation(n=1):
    counter_win = 0
    for index in range(n):
        if algorithm(prices_to_win) == 1:
            counter_win += 1
    return counter_win


print(simulation(10000))
